== README

This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

* Ruby version
- I've developed it using ruby 2.2.3, but any version in the 2.2 series or higher should work.

* System dependencies
- I would use Bundler and a gemset for this in the following way:
-- 1. rvm use 2.2.3@lists
-- 2. gem install bundler --no-ri --no-rdoc
-- 3. bundle

* Configuration
- none, necessary

* Database creation
- env RAILS_ENV=production ./bin/rake db:create

* Database initialization
- env RAILS_ENV=production ./bin/rake db:migrate

* Deployment instructions
- env RAILS_ENV=production ./bin/rails s

